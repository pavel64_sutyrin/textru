from rest_framework import serializers

from app.review.models import Review, ReviewVerification

from textru.settings import TEXTRU_RESULT_URL


class ReviewVerificationSerializer(serializers.ModelSerializer):

    class Meta:

        model = ReviewVerification


class ReviewSerializer(serializers.ModelSerializer):

    status = serializers.SerializerMethodField()
    verification = serializers.SerializerMethodField()

    class Meta:

        model = Review

        fields = (
            'id', 'object_name', 'comment', 'created_at', 'updated_at',
            'status', 'visible', 'verification'
        )

        read_only_fields = (
            'id', 'created_at', 'updated_at', 'status', 'visible',
            'verification'
        )

    @staticmethod
    def get_status(obj):
        return obj.get_status_display()

    @staticmethod
    def get_verification(obj):
        verification = {}
        if obj.status == obj.NEW or \
                not hasattr(obj, 'reviewverification'):
            return None

        if obj.status in (obj.IN_VERIFICATION, obj.VERIFIED):
            verification['text_unique'] = obj.reviewverification.text_unique
            verification['url'] =\
                TEXTRU_RESULT_URL.format(text_uid=obj.reviewverification.uuid)

        elif obj.status in (obj.FAILED, obj.ERROR):
            verification['error_code'] = obj.reviewverification.error_code
            verification['error_desc'] = obj.reviewverification.error_desc

        if verification:
            if obj.reviewverification.start_at:
                verification['start_at'] = obj.reviewverification.start_at
            if obj.reviewverification.end_at:
                verification['end_at'] = obj.reviewverification.end_at

        return verification or None
