import logging

from django.utils import timezone
from django.http.response import HttpResponse, JsonResponse
from django.utils.dateparse import parse_date, parse_datetime
from django.utils.translation import ugettext_lazy as _

from rest_framework import mixins
from rest_framework.exceptions import NotFound
from rest_framework.viewsets import GenericViewSet
from rest_framework.decorators import detail_route, list_route

from app.review.models import Review, ReviewVerification
from app.review.serializer import ReviewSerializer


log = logging.getLogger(__name__)


class ReviewViewSet(mixins.RetrieveModelMixin,
                    mixins.ListModelMixin,
                    mixins.CreateModelMixin,
                    GenericViewSet):

    queryset = Review.objects.all()
    serializer_class = ReviewSerializer
    permission_classes = []

    def get_queryset(self):
        return self.queryset.select_related('reviewverification')

    @staticmethod
    def __get_list_verificvation(request, is_send_to_verification=False):
        date = request.GET.get('date')
        if date:
            date = parse_date(date)
        else:
            date = None

        reviews = Review.get_all_review_for_verification(date)
        response = []

        for review in reviews:
            if is_send_to_verification:
                # Run verification
                when = request.GET.get('when')
                if when:
                    when = parse_datetime(when)
                else:
                    when = None
                review.run_verification(when=when)

            response.append(review.as_dict())

        return response

    @list_route(methods=['get'], url_path='view-verificate')
    def view_verificate(self, request):
        return JsonResponse(self.__get_list_verificvation(
            request,
            is_send_to_verification=False,
        ), safe=False)

    @list_route(methods=['get'], url_path='verificate')
    def verificate(self, request):
        return JsonResponse(self.__get_list_verificvation(
            request,
            is_send_to_verification=True
        ), safe=False)

    @detail_route(methods=['get'], url_path='verificate')
    def verificate_obj(self, request, pk):
        try:
            r = Review.objects.get(pk=pk)
        except Review.DoesNotExist:
            raise NotFound

        if r.status not in [Review.NEW, Review.ERROR]:
            return JsonResponse(
                {'detail': _('This Review is already virefied or '
                             'in verification.')}, status=400)

        when = request.GET.get('when')
        if when:
            when = parse_datetime(when)
        else:
            when = None

        if not r.run_verification(when=when):
            return JsonResponse(
                {'detail': _('Review was not send to verification.')},
                status=400)

        return JsonResponse({'detail': _('Success')}, status=400)


def textru_result_callback(request, callback_uuid):
    post = request.POST or {}
    if not post:
        return HttpResponse('ok')

    uid = post.get('uid')
    text_unique = post.get('text_unique')
    json_result = post.get('json_result')
    spell_check = post.get('spell_check')
    error_code = post.get('error_code')
    error_desc = post.get('error_desc')

    try:
        rv = ReviewVerification.objects.get(
            uuid=uid,
            callback_uuid=callback_uuid
        )
    except (ReviewVerification.DoesNotExist,
            ReviewVerification.MultipleObjectsReturned,
            ValueError) as e:
        log.error(str(e))
        return HttpResponse('ok')

    if text_unique:
        rv.text_unique = text_unique
    if json_result:
        rv.json_result = json_result
    if spell_check:
        rv.spell_check = spell_check
    if error_code or \
            error_desc:
        try:
            rv.error_code = int(error_code) or 0
        except ValueError:
            rv.error_code = 0

        rv.error_desc = error_desc

        # Set FAILED status for review
        rv.review.status = rv.review.FAILED
        rv.review.save()

        rv.end_at = timezone.now()
    else:
        if spell_check:
            # Set VERIFIED status for review
            rv.review.status = rv.review.VERIFIED
            rv.end_at = timezone.now()
            rv.review.save()

    rv.save()

    return HttpResponse('ok')
