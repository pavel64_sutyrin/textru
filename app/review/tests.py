# -*- coding: utf8
from django.test import TestCase

import pytest
from models import Review
from tasks import review_verification, get_verification_result
import mock
from requests import Response
import json


class TestVerify(TestCase):
    @mock.patch('requests.post')
    def test_send(self, mock_post=None):
        review = Review.objects.create(
            comment=u"""
            Он говорил на том изысканном французском языке,
            на котором не только говорили, но и думали наши деды,
             и с теми, тихими, покровительственными интонациями,
              которые свойственны состаревшемуся в свете
              и при дворе значительному человеку.
              Он подошел к Анне Павловне, поцеловал ее руку,
              подставив ей свою надушенную и сияющую лысину,
              и покойно уселся на диване.""",
            comment_len=300,
        )
        assert review

        if mock_post:
            class res1:
                pass

            res1.status_code = 200
            res1.content = '{"text_uid":"57bcef838e38d"}'
            class res2:
                pass

            res2.status_code = 200
            res2.content = """{"error_code":181,
                "error_desc":"\u0422\u0435\u043a\u0441\u0442 \u0435\u0449\u0451 \u043d\u0435 \u043f\u0440\u043e\u0432\u0435\u0440\u0435\u043d",
                "queuetext":"1","queueproc":"0"}'"""
            mock_post.side_effect = [
                res1,
                res2
            ]

        verification = review_verification(review.id)
        assert verification.text_uid == u'57bcef838e38d'
        # assert verification.status == Review.IN_VERIFICATION
        verification = get_verification_result(review.id)
        # assert verification.text_unique == '100'
