from textru.settings import SEND_ON_VERIFICATION_AFTER_ADD_REVIEW


def review_pre_save(sender, **kwargs):
    if not kwargs['instance'].pk:
        kwargs['instance'].status = sender.NEW
        kwargs['instance'].comment_len = len(kwargs['instance'].comment)


def review_post_save(sender, **kwargs):
    if 'created' in kwargs and \
            kwargs['created'] and \
            kwargs['instance'].status == sender.NEW:
        if SEND_ON_VERIFICATION_AFTER_ADD_REVIEW:
            kwargs['instance'].run_verification()
