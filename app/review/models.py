from __future__ import unicode_literals

from datetime import datetime, timedelta, time

from django.db import models
from django.utils.translation import ugettext_lazy as _

from textru.settings import COMMENT_MIN_LENGTH_FOR_VERIFICATION

from app.review import signals
from app.review.tasks import review_verification


class Review(models.Model):

    NEW, \
        IN_VERIFICATION, \
        VERIFIED, \
        FAILED,\
        ERROR = range(1, 6)

    STATUS_TYPES = (
        (NEW, _('New')),
        (IN_VERIFICATION, _('In the verification')),
        (VERIFIED, 'Verified'),
        (FAILED, 'Failed verification'),
        (ERROR, 'Error')
    )

    object_name = models.CharField(
        _('Object name'),
        max_length=50,
        db_index=True
    )

    comment = models.TextField(
        _('Comment')
    )

    comment_len = models.IntegerField(
        _('Comment length'),
        db_index=True
    )

    created_at = models.DateTimeField(
        _('Created at'),
        auto_now=True,
        db_index=True
    )

    updated_at = models.DateTimeField(
        _('Created at'),
        auto_now_add=True,
        db_index=True
    )

    status = models.PositiveSmallIntegerField(
        _('Status'),
        choices=STATUS_TYPES,
        default=NEW,
        db_index=True
    )

    visible = models.BooleanField(
        _('Visible'),
        default=False,
        db_index=True
    )

    def __str__(self):
        return '#' + str(self.pk) + ' ' + self.object_name

    def run_verification(self, when=None):
        """
        Run verification.
        :param when: run task in datetime default now.
        :return: run id
        """
        return review_verification.apply_async([self.pk], eta=when)

    @classmethod
    def get_all_review_for_verification(cls, date=None):
        if not date:
            date = datetime.now().date()

        return cls.objects.filter(
            # This is filter by object and comment len greater then 100.
            # Replace object_name in future.
            # __isnull - Never will not work.
            models.Q(object_name__isnull=True) |
            models.Q(comment_len__gte=COMMENT_MIN_LENGTH_FOR_VERIFICATION),
            visible=False,
            created_at__date=date,
            status__in=[cls.NEW, cls.ERROR]
        )

    def as_dict(self, with_verification=False):
        data = {
            'object_name': self.object_name,
            'comment': self.comment,
            'comment_len': self.comment_len,
            'created_at': self.created_at,
            'updated_at': self.updated_at,
            'status': self.get_status_display(),
            'visible': self.visible,
        }

        if with_verification:
            data['verification'] = self.reviewverification.as_dict()

        return data


# models.signals.pre_save.connect(
#     receiver=signals.review_pre_save,
#     sender=Review,
#     weak=False,
#     dispatch_uid='review_pre_save'
# )
#
# models.signals.post_save.connect(
#     receiver=signals.review_post_save,
#     sender=Review,
#     weak=False,
#     dispatch_uid='review_post_save'
# )


class ReviewVerification(models.Model):

    review = models.OneToOneField(
        'Review',
        on_delete=models.CASCADE,
        primary_key=True,
        verbose_name=_('Review')
    )

    start_at = models.DateTimeField(
        _('Verification started at'),
        null=True,
        blank=True,
        db_index=True
    )

    end_at = models.DateTimeField(
        _('Verification completed at'),
        null=True,
        blank=True,
        db_index=True
    )

    callback_uuid = models.UUIDField(
        _('Callback UUID key'),
        null=True,
        blank=True,
        db_index=True
    )

    text_uid = models.TextField(
        _('Text UID'),
        max_length=100,
        null=True,
        blank=True,
        db_index=True
    )

    text_unique = models.DecimalField(
        _('Text unique percent'),
        null=True,
        blank=True,
        db_index=True,
        max_digits=5,
        decimal_places=2
    )

    result_json = models.TextField(
        _('Result json'),
        null=True,
        blank=True
    )

    spell_check = models.TextField(
        _('Result json'),
        null=True,
        blank=True
    )

    seo_check = models.TextField(
        _('Result json'),
        null=True,
        blank=True
    )

    error_code = models.PositiveSmallIntegerField(
        _('Error code'),
        null=True,
        blank=True
    )

    error_desc = models.CharField(
        _('Error description'),
        max_length=512,
        null=True,
        blank=True
    )

    def __str__(self):
        return str(self.review) + ' Verification ' + (self.uuid or '')
