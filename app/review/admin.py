from django.contrib import admin

from app.review.models import Review, ReviewVerification


class ReviewAdmin(admin.ModelAdmin):
    list_filter = ['status', 'visible']


class ReviewVerificationAdmin(admin.ModelAdmin):
    list_filter = []


admin.site.register(Review, ReviewAdmin)
admin.site.register(ReviewVerification, ReviewVerificationAdmin)
