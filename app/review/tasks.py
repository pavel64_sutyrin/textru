import logging
import requests
import json

from celery import task

from uuid import uuid4

from django.urls import reverse_lazy
from django.contrib.sites.models import Site
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from textru.settings import TEXTRU_URL, TEXTRU_USER_KEY, \
    TEXTRU_PUBLIC_RESULTS, USE_HTTPS

log = logging.getLogger('Celery')


@task(name='review_verification')
def review_verification(review_id):
    from app.review.models import Review, ReviewVerification

    try:
        review = Review.objects.get(pk=review_id)
    except Review.DoesNotExist:
        return False

    current_site = Site.objects.get_current()

    verification = ReviewVerification()
    verification.review = review
    verification.start_at = timezone.now()
    verification.callback_uuid = str(uuid4())

    try:
        response = requests.post(
            TEXTRU_URL,
            data={
                'text': review.comment,
                'userkey': TEXTRU_USER_KEY,
                'visible': 'vis_on' if TEXTRU_PUBLIC_RESULTS else '',
                'callback': '%s://%s/%s/' % (
                    'https' if USE_HTTPS else 'http',
                    current_site.domain,
                    reverse_lazy(
                        'textru_callback',
                        args=(str(verification.callback_uuid),)
                    )
                )
            }
        )
        if response.status_code != 200:
            raise Exception(_('The `text.ru` is not available.'))

        try:
            data = json.loads(response.content)
            # print response.content
            log.debug(data)
        except Exception as e:
            raise Exception('Error in `text.ru` response. Details: %s'.format(
                str(e)
            ))

    except Exception as e:
        log.error(e)

        review.status = review.ERROR
        verification.end_at = timezone.now()
        verification.error_code = 0
        verification.error_desc = str(e)

    else:
        if 'text_uid' not in data and \
                        'error_code' in data:
            review.status = review.ERROR

            try:
                error_code = int(data['error_code'])
            except ValueError:
                error_code = 0

            verification.error_code = error_code
            verification.error_desc = data['error_desc']
        else:
            review.status = review.IN_VERIFICATION
            verification.text_uid = data['text_uid']

    review.save()
    verification.save()
    return verification


@task(name='get_verification_result')
def get_verification_result(review_id):
    from app.review.models import Review, ReviewVerification
    from django.conf import settings
    try:
        review = Review.objects.get(id=review_id)
        verification = ReviewVerification.objects.get(review=review)

    except ReviewVerification.DoesNotExist:
        pass

    current_site = Site.objects.get_current()

    try:
        response = requests.post(
            TEXTRU_URL,
            data={
                'uid': verification.text_uid,
                'userkey': TEXTRU_USER_KEY,
            }
        )
        if response.status_code != 200:
            raise Exception(_('The `text.ru` is not available.'))

        try:
            data = json.loads(response.content)
            # print response.content
            log.debug(data)
        except Exception as e:
            raise Exception('Error in `text.ru` response. Details: %s'.format(
                str(e)
            ))

    except Exception as e:
        log.error(e)

        review.status = review.ERROR
        verification.end_at = timezone.now()
        verification.error_code = 0
        verification.error_desc = str(e)

    else:
        verification.text_unique = data['text_unique']
        verification.text_json = data['text_json']
        verification.spell_check = data['spell_check']
        verification.seo_check = data['seo_check']
        if 'text_unique' not in data and \
                        'error_code' in data:
            review.status = review.ERROR

            try:
                error_code = int(data['error_code'])
            except ValueError:
                error_code = 0

            if error_code == settings.TEXTRU_ERROR_CODE_IN_VERIFICATION:
                review.status = review.IN_VERIFICATION
            else:
                verification.error_code = error_code
                verification.error_desc = data['error_desc']

    review.save()
    verification.save()
    return verification
