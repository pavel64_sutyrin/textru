To run tests:

`./manage.py test`

To run celery:

`celery --app=textru.settings_celery:celery_app worker --loglevel=INFO`