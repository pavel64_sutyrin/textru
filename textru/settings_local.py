from textru.settings import INSTALLED_APPS, BASE_DIR
import os

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db', 'db.sqlite3'),
    }
}

INSTALLED_APPS += [
    'debug_toolbar'
]
