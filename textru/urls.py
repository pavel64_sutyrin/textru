from django.conf.urls import url, include
from django.contrib import admin

from rest_framework.routers import DefaultRouter

from app.review.views import ReviewViewSet, textru_result_callback


router = DefaultRouter()
router.register(r'review', ReviewViewSet)


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include(router.urls)),
    url(r'^text.ru-callback/(?P<callback_uuid>[\w\-]+)/$',
        textru_result_callback, name='textru_callback')
]
